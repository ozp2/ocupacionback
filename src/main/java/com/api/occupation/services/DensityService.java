package com.api.occupation.services;

import static com.api.occupation.utils.ConstantsUtil.DATE_FORMAT_WH;
import static com.api.occupation.utils.ConstantsUtil.DENSITY_COLLECTION;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.api.occupation.filters.BsonAgregates;
import com.api.occupation.models.DateRange;
import com.api.occupation.models.density.ChartData;
import com.api.occupation.models.density.Density;
import com.api.occupation.models.exceptions.RestTemplateResponseErrorHandler;
import com.api.occupation.utils.ConstantsUtil;
import com.api.occupation.utils.DateUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

@Service
public class DensityService {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private DBService dbService;

	public List<Density> getDensityIOnBeach(String idCam, DateRange date) {
		List<Density> density = null;
		ResponseEntity<String> response = null;
		try {

			response = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build()
					.exchange(getURL(idCam, date), HttpMethod.GET, null, String.class);
			density = objectMapper.readValue(response.getBody(), new TypeReference<List<Density>>() {
			});

		} catch (IOException e) {
		} catch (com.api.occupation.exception.NotFoundException e) {
		}
		return density;
	}

	public List<Density> getDensityMongoDB(String idCam) {
		List<Density> densityList = new ArrayList<>();
		Density density = null;
		MongoCollection<Document> collection = dbService.getCollection(DENSITY_COLLECTION);
		Document document = BsonAgregates.getLastDocumentByTimestamp(collection);

		String json = document.toJson();
		try {
			density = objectMapper.readValue(json, new TypeReference<Density>() {
			});
			densityList.add(density);
		} catch (IOException e) {
		}
		return densityList;
	}

	private String getURL(String idCam, DateRange date) {
		String url = ConstantsUtil.DENSITY_URL.replaceAll(ConstantsUtil.ID_BEACH, idCam);
		if (date.getInitdatetime() != null && !date.getInitdatetime().isEmpty()) {
			url = url + "?initdatetime=" + date.getInitdatetime();
		}
		if (date.getEnddatetime() != null && !date.getEnddatetime().isEmpty()) {
			if (date.getInitdatetime() != null && !date.getInitdatetime().isEmpty()) {
				url = url + "&enddatetime=" + date.getEnddatetime();
			} else {
				url = url + "?enddatetime=" + date.getEnddatetime();
			}
		}
		return url;
	}

	public List<ChartData> getChartData(String beachId, int record) {
		List<ChartData> chartDataList = new ArrayList<>();

		MongoCollection<Document> collection = dbService.getCollection(DENSITY_COLLECTION);
		FindIterable<Document> documentList = BsonAgregates.getLastDocumentByTimestamp(collection, beachId, record);
		ArrayList<Document> arrayList = new ArrayList<Document>();
		for (Document document : documentList) {
			arrayList.add(document);
		}
		Collections.reverse(arrayList);

		for (int i = 0; i < 24; i++) {
			chartDataList.add(new ChartData());
		}
		for (Document document : arrayList) {
			String json = document.toJson();
			Density density = null;
			try {
				density = objectMapper.readValue(json, new TypeReference<Density>() {
				});
			} catch (IOException e) {
			}
			for (int i = 0; i < 24; i++) {
				if (chartDataList.get(i).getOccupation() == null) {
					chartDataList.get(i).setOccupation(new ArrayList<>());
				}
				chartDataList.get(i).getOccupation().add(density.getCells().get(i).getValue() * 100);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_WH);
				Date date = null;
				try {
					date = simpleDateFormat.parse(DateUtils.getDateFromTimestamp(density.getTimestamp()) + "-"
							+ DateUtils.getHourFromTimestamp(density.getTimestamp()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (chartDataList.get(i).getDate() == null) {
					chartDataList.get(i).setDate(new ArrayList<>());
				}
				chartDataList.get(i).getDate().add(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

				if (chartDataList.get(i).getHumidity() == null) {
					chartDataList.get(i).setHumidity(new ArrayList<>());
				}

				chartDataList.get(i).getHumidity().add(getRandom(50.0, 70.0));
				
				  if (chartDataList.get(i).getTemperature() == null) {
				  chartDataList.get(i).setTemperature(new ArrayList<>()); }
				  
				  chartDataList.get(i).getTemperature().add(getRandom(20.0, 35.0));
				 
				if (chartDataList.get(i).getRain() == null) {
					chartDataList.get(i).setRain(new ArrayList<>());
				}
				chartDataList.get(i).getRain().add(getRandom(0.0, 1.0));

			}
		}

		return chartDataList;
	}

	private static double getRandom(Double valorMinimo, Double valorMaximo) {
		Random rand = new Random();
		return valorMinimo + (valorMaximo - valorMinimo) * rand.nextDouble();
	}

}
