package com.api.occupation.services;

import static com.api.occupation.utils.ConstantsUtil.DATE_FORMAT;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.occupation.models.BeachData;
import com.api.occupation.models.DataHeatMap;
import com.api.occupation.models.DateRange;
import com.api.occupation.models.ZoneData;
import com.api.occupation.models.density.Cell;
import com.api.occupation.models.density.Density;
import com.api.occupation.models.zones.ZoneList;
import com.api.occupation.models.zones.Zones;
import com.api.occupation.utils.DateUtils;
import com.api.occupation.utils.DensityUtil;
import com.api.occupation.utils.MathUtils;

@Service
public class DataByZoneService {

	@Autowired
	private ZonesService zonesService;
	@Autowired
	private DensityService densityService;

	public BeachData getDataByZone(String idCam, DateRange date) {
		List<Density> densityList = densityService.getDensityIOnBeach(idCam, date);
		if(densityList == null || densityList.isEmpty()) {
			densityList = densityService.getDensityMongoDB(idCam);
		}
		BeachData beachData = new BeachData();
		if (densityList != null)  {
			beachData.setZoneDataList(getDensitiesByZones(zonesService.getZonesIONBeach(idCam), densityList));
			beachData.setDateTime(getTimestamp(densityList));
			beachData.setTotalDensity(getTotalDensity(densityList));
		}
		return beachData;
	}
	
	public List<DataHeatMap> getHeatMapData(String idCam, DateRange date) {
		return getHeatMap(idCam, date);
	}
	
	private LocalDateTime getTimestamp(List<Density> densityList) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = null;
		try {
			date = simpleDateFormat.parse(DateUtils.getDateFromTimestamp(densityList.get(0).getTimestamp())+"-"+DateUtils.getHourFromTimestamp(densityList.get(0).getTimestamp()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
	
	private Double getTotalDensity(List<Density> densityList) {
		return MathUtils.roundDensity(densityList.get(0).getOccupation());
	}

	private List<ZoneData> getDensitiesByZones(Zones zones, List<Density> density) {
		List<ZoneData> listZoneData = new ArrayList<>();
		if(density != null) {
			ZoneData data = null;
			for (ZoneList zone : zones.getZonelist()) {
				if(!zone.getZoname().equals("posR1")) {
					data = new ZoneData();
					data.setDensity(getDensity(DensityUtil.getDensityAverage(density).getCells(), zone.getZoname()));
					data.setZoneList(zone);
					listZoneData.add(data); 
				}
			}
		}
		return listZoneData;
	}

	private double getDensity(List<Cell> density, String zoname) {
		for (int i = 0; i < density.size(); i++) {
			if (density.get(i).getId().equals(zoname)) {
				return MathUtils.roundDensity(density.get(i).getValue());
			}
		}
		return 0;
	}
	
	private List<DataHeatMap> getHeatMap(String idBeach, DateRange date) {
		List<DataHeatMap> heatMap = new ArrayList<>();
		DataHeatMap data = null;;

		List<ZoneData> listZoneData = getDensitiesByZones(zonesService.getZonesIONBeach(idBeach), densityService.getDensityIOnBeach(idBeach, date));
		for(ZoneData zd : listZoneData) {
			data = new DataHeatMap();
			data.setDensity(zd.getDensity()*10);
			data.setLatitude(zd.getZoneList().getLat11());
			data.setLongitude(zd.getZoneList().getLng11());
			if(!heatMap.stream()
					.filter(hm->hm.getLatitude()
							.equals(zd.getZoneList().getLat11())
							&&hm.getLongitude().equals(zd.getZoneList().getLng11()))
					.findFirst().isPresent()) {
				heatMap.add(data);
			}			
			data = new DataHeatMap();
			data.setDensity(zd.getDensity()*10);
			data.setLatitude(zd.getZoneList().getLat12());
			data.setLongitude(zd.getZoneList().getLng12());
			if(!heatMap.stream()
					.filter(hm->hm.getLatitude()
							.equals(zd.getZoneList().getLat12())
							&&hm.getLongitude().equals(zd.getZoneList().getLng12()))
					.findFirst().isPresent()) {
				heatMap.add(data);
			}
			data = new DataHeatMap();
			data.setDensity(zd.getDensity()*10);
			data.setLatitude(zd.getZoneList().getLat21());
			data.setLongitude(zd.getZoneList().getLng21());
			if(!heatMap.stream()
					.filter(hm->hm.getLatitude()
							.equals(zd.getZoneList().getLat21())
							&&hm.getLongitude().equals(zd.getZoneList().getLng21()))
					.findFirst().isPresent()) {
				heatMap.add(data);
			}
			data = new DataHeatMap();
			data.setDensity(zd.getDensity()*10);
			data.setLatitude(zd.getZoneList().getLat22());
			data.setLongitude(zd.getZoneList().getLng22());
			if(!heatMap.stream()
					.filter(hm->hm.getLatitude()
							.equals(zd.getZoneList().getLat22())
							&&hm.getLongitude().equals(zd.getZoneList().getLng22()))
					.findFirst().isPresent()) {
				heatMap.add(data);
			}
		}
		return heatMap;
	}
}
