package com.api.occupation.services;

import java.io.IOException;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.api.occupation.filters.BsonAgregates;
import com.api.occupation.models.DateRange;
import com.api.occupation.models.Weather;
import com.api.occupation.models.exceptions.RestTemplateResponseErrorHandler;
import com.api.occupation.models.mastral.Actuales;
import com.api.occupation.models.mastral.Proyecto_Mastral_XML_V1;
import com.api.occupation.utils.ConstantsUtil;
import com.api.occupation.utils.DateUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mongodb.client.MongoCollection;

@Service
public class WeatherService {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	@Autowired
	private DensityService densityService;
	@Autowired
	private DBService dbService;
	@Autowired
	private ObjectMapper objectMapper;
	private XmlMapper xmlMapper = new XmlMapper();

	public Proyecto_Mastral_XML_V1 getMastral() {
		Proyecto_Mastral_XML_V1 mastral = null;
		ResponseEntity<String> response = null;
		try {
			response = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build()
					.exchange(ConstantsUtil.MASTRAL_URL, HttpMethod.GET, null, String.class);
			mastral = xmlMapper.readValue(response.getBody(), Proyecto_Mastral_XML_V1.class);

		} catch (Exception e) {
			System.out.println(e);
		}
		return mastral;
	}

	public Weather getActuales() {
		Proyecto_Mastral_XML_V1 mastral = getMastral();
		return new Weather(mastral.getActuales(), mastral.getFecha(), mastral.getHora(), mastral.getEstacion());
	}

	public Actuales getWeatherByDate(String idCam, String initdatetime, String enddatetime) {
		Integer timestamp;
		String date1;
		String date2;
		String hour1;
		String hour2;
		if (initdatetime == null && enddatetime == null) {
			timestamp = densityService.getDensityIOnBeach(idCam, new DateRange()).get(0).getTimestamp();
			date1 = DateUtils.getDateFromTimestamp(timestamp);
			date2 = DateUtils.getDateFromTimestamp(timestamp);
			hour1 = DateUtils.getHourFromTimestamp(timestamp);
			hour2 = DateUtils.getHourFromTimestamp(timestamp);
		} else {
			date1 = DateUtils.getDateFromTimestamp(Integer.valueOf(initdatetime));
			date2 = DateUtils.getDateFromTimestamp(Integer.valueOf(enddatetime));
			hour1 = DateUtils.getHourFromTimestamp(Integer.valueOf(initdatetime));
			hour2 = DateUtils.getHourFromTimestamp(Integer.valueOf(enddatetime));
		}
		return getMastralFromDatabase(idCam, date1, date2, hour1, hour2);
	}

	private Actuales getMastralFromDatabase(String idCam, String date1, String date2, String hour1, String hour2) {
		Actuales actuales = getDefaultActuales();

		MongoCollection<Document> collection = dbService.getCollection(ConstantsUtil.MASTRAL_ACTUALES_COLLECTION);
		Document document = BsonAgregates.getDocumentsByDateAndHour(collection, date1, date2, hour1, hour2);
		if (document != null) {
			String json = document.toJson();
			try {
				actuales = objectMapper.readValue(json, new TypeReference<Actuales>() {
				});
			} catch (IOException e) {
			}
		}
		return actuales;
	}

	private Actuales getDefaultActuales() {
		Actuales actuales = new Actuales();
		actuales.setDireccionviento(null);
		actuales.setHumedad(null);
		actuales.setIntensidadlluvia(null);
		actuales.setLluvia(null);
		actuales.setLluviaultimahora(null);
		actuales.setPresion(null);
		actuales.setVelocidadviento(null);
		actuales.setPuntoderocio(null);
		actuales.setTemperatura(null);
		actuales.setEvaporacion(null);
		actuales.setRadiacionsolar(null);
		actuales.setIndiceuv(null);
		return actuales;
	}
}
