package com.api.occupation.services;

import static com.api.occupation.utils.ConstantsUtil.DB_BEACH;
import static com.api.occupation.utils.ConstantsUtil.ZONES_COLLECTION;

import java.io.IOException;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.api.occupation.filters.BsonFilters;
import com.api.occupation.models.exceptions.RestTemplateResponseErrorHandler;
import com.api.occupation.models.zones.Zones;
import com.api.occupation.utils.ConstantsUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

@Service
public class ZonesService {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private DBService dbService;

	public Zones getZonesIONBeach(String idCam) {
		Zones zones = null;
		ResponseEntity<String> response = null;
		String url = ConstantsUtil.ZONES_URL.replaceAll(ConstantsUtil.ID_BEACH, idCam);
		try {
			response = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build().exchange(url,
					HttpMethod.GET, null, String.class);
			zones = objectMapper.readValue(response.getBody(), Zones.class);

		} catch (Exception e) {
			System.out.println(e);
		}
		return zones;
	}

	public Zones getZonesMongoDB(String idCam) {
		MongoCollection<Document> collection = dbService.getCollection(ZONES_COLLECTION);
		FindIterable<Document> cursor = collection.find(BsonFilters.filterEquals(DB_BEACH, idCam));
		Zones zones = null;

		Document doc = cursor.first();
		String json = doc.toJson();
		try {
			zones = objectMapper.readValue(json, new TypeReference<Zones>() {});
		} catch (IOException e) {
		}
		return zones;
	}
}
