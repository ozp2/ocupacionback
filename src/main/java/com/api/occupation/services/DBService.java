package com.api.occupation.services;

import static com.api.occupation.utils.ConstantsUtil.MONGO_DB_HOST;
import static com.api.occupation.utils.ConstantsUtil.OCUPATION_DB;

import org.bson.Document;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;;

@Service
public class DBService {

	public MongoCollection<Document> getCollection(String collection) {
		MongoClient mongoClient = MongoClients.create(MONGO_DB_HOST);
		MongoDatabase database = mongoClient.getDatabase(OCUPATION_DB);
		return database.getCollection(collection);
	}

}
