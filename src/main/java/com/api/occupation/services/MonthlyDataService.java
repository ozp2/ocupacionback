package com.api.occupation.services;

import static com.api.occupation.utils.ConstantsUtil.*;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.occupation.filters.BsonAgregates;
import com.api.occupation.models.DayData;
import com.api.occupation.utils.DateUtils;
import com.api.occupation.utils.MathUtils;
import com.mongodb.client.MongoCollection;;

@Service
public class MonthlyDataService {

	@Autowired
	private DBService dbService;

	public List<DayData> getMonthData() {

		MongoCollection<Document> collection = dbService.getCollection(DENSITY_COLLECTION);
		MongoCollection<Document> collectionMastral = dbService.getCollection(MASTRAL_DIARIOS_COLLECTION);

		int monthsDays = DateUtils.getDayMonths();
		int currentDay = DateUtils.getDayOfMonth();

		List<DayData> monthData = new ArrayList<>();
		DayData dayData = null;

		for (int i = 0; i < monthsDays; i++) {
			dayData = new DayData();
			dayData.setDay(i + 1);

			if (i < currentDay) {
				Document documentDensity = BsonAgregates.getMax(collection, DateUtils.getDateToQuery(i + 1), DATE_FIELD,
						OCCUPATION_FIELD);
		
				if (documentDensity != null) {
					dayData.setDensity(MathUtils.roundDensity(documentDensity.getDouble(OCCUPATION_FIELD)));

					Document documentTempMax = BsonAgregates.getMax(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MAX_TEMP_FIELD);
					dayData.setTempMax(documentTempMax != null ? documentTempMax.get(MAX_TEMP_FIELD).toString()
							: DEFAULT_MAX_TEMP);

					Document documentTempMin = BsonAgregates.getMin(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MIN_TEMP_FIELD);
					dayData.setTempMin(documentTempMin != null ? documentTempMin.get(MIN_TEMP_FIELD).toString()
							: DEFAULT_MIN_TEMP);

					Document documentHumMin = BsonAgregates.getMin(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MIN_HUM_FIELD);
					dayData.setHumMin(
							documentHumMin != null ? documentHumMin.get(MIN_HUM_FIELD).toString() : DEFAULT_MIN_HUM);

					Document documentHumMax = BsonAgregates.getMax(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MAX_HUM_FIELD);
					dayData.setHumMax(
							documentHumMax != null ? documentHumMax.get(MAX_HUM_FIELD).toString() : DEFAULT_MAX_HUM);

					Document documentRain = BsonAgregates.getMax(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, RAIN_FIELD);
					dayData.setRain(documentRain != null ? documentRain.get(RAIN_FIELD).toString() : DEFAULT_RAIN);

					Document documentRadMax = BsonAgregates.getMax(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MAX_RAD_FIELD);
					dayData.setRadMax(documentRadMax != null && documentRadMax.get(MAX_RAD_FIELD) != null
							? documentRadMax.get(MAX_RAD_FIELD).toString()
							: DEFAULT_MAX_RAD);

					Document documentEvMax = BsonAgregates.getMax(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MAX_EVAP_FIELD);
					dayData.setEvapMax(documentEvMax != null && documentRadMax.get(MAX_EVAP_FIELD) != null
							? documentEvMax.get(MAX_EVAP_FIELD).toString()
							: DEFAULT_MAX_EVAP);

					Document documentUvMax = BsonAgregates.getMax(collectionMastral, DateUtils.getDateToQuery(i + 1),
							DB_DATE, MAX_UV_FIELD);
					dayData.setUvMax(documentUvMax != null && documentRadMax.get(MAX_UV_FIELD) != null
							? documentUvMax.get(MAX_UV_FIELD).toString()
							: DEFAULT_MAX_UV);

				} else {
					dayData.setDensity(DEFAULT_DENSITY);
					dayData.setTempMax(DEFAULT_MAX_TEMP);
					dayData.setTempMin(DEFAULT_MIN_TEMP);
					dayData.setHumMax(DEFAULT_MAX_HUM);
					dayData.setHumMin(DEFAULT_MIN_HUM);
					dayData.setRain(DEFAULT_RAIN);
					dayData.setEvapMax(DEFAULT_MAX_EVAP);
					dayData.setUvMax(DEFAULT_MAX_UV);
					dayData.setRadMax(DEFAULT_MAX_RAD);
				}

			} else {
				dayData.setDensity(DEFAULT_DENSITY);
				dayData.setTempMax(DEFAULT_MAX_TEMP);
				dayData.setTempMin(DEFAULT_MIN_TEMP);
				dayData.setHumMax(DEFAULT_MAX_HUM);
				dayData.setHumMin(DEFAULT_MIN_HUM);
				dayData.setRain(DEFAULT_RAIN);
				dayData.setEvapMax(DEFAULT_MAX_EVAP);
				dayData.setUvMax(DEFAULT_MAX_UV);
				dayData.setRadMax(DEFAULT_MAX_RAD);
			}
			monthData.add(dayData);
		}
		return monthData;
	}
}
