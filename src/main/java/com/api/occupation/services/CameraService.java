package com.api.occupation.services;

import static com.api.occupation.utils.ConstantsUtil.CAMERAS_COLLECTION;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.occupation.models.Camera;
import com.api.occupation.models.CameraSelect;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;;

@Service
public class CameraService {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private DBService dbService;

	public List<CameraSelect> getCameras() {
		List<CameraSelect> beachList = new ArrayList<>();
		Camera camera = null;
		CameraSelect cameraSelect = null;

		MongoCollection<Document> collection = dbService.getCollection(CAMERAS_COLLECTION);
		MongoCursor<Document> cur = collection.find().iterator();

		while (cur.hasNext()) {
			Document doc = cur.next();
			String json = doc.toJson();
			try {
				camera = objectMapper.readValue(json, new TypeReference<Camera>() {
				});
				cameraSelect = new CameraSelect();
				cameraSelect.setLabel(camera.getName());
				cameraSelect.setValue(camera.getCameraId());
				beachList.add(cameraSelect);
			} catch (IOException e) {
			}
		}
		return beachList;
	}
}
