package com.api.occupation.utils;

public class ConstantsUtil {
	public static final String ID_BEACH = "idBeach";
	public static final String ZONES_URL = "http://apicet.westeurope.cloudapp.azure.com:7500/api/beachcams/idBeach/params";
	public static final String DENSITY_URL = "http://apicet.westeurope.cloudapp.azure.com:7000/ion_beach/density/idBeach";
	public static final String MASTRAL_URL = "http://www.eltiempoenloslocos.com/loslocos";
	
	public static final String MONGO_DB_HOST = "mongodb://localhost:27017";
	public static final String OCUPATION_DB = "ocupation";
	public static final String CAMERAS_COLLECTION = "cameras";
	public static final String ZONES_COLLECTION = "zones";
	public static final String DENSITY_COLLECTION = "density";
	public static final String MASTRAL_ACTUALES_COLLECTION = "mastral-actuales";
	public static final String MASTRAL_DIARIOS_COLLECTION = "mastral-diarios";
	public static final String MASTRAL_MENSUALES_COLLECTION = "mastral-mensuales";
	public static final String MASTRAL_ANUALES_COLLECTION = "mastral-anuales";
	
	public static final String DB_DATE= "fecha";
	public static final String DB_HOUR= "hora";
	public static final String DB_TIME= "timestamp";
	public static final String DB_BEACH= "_idcam";
	public static final String DB_CAMERA= "cameraId";
	public static final String DB_BEACH_ID= "beachId";
	
	
	public static final String NO_DISPONIBLE= "No disponible";
	
	public static final String DATE_FORMAT= "dd/MM/yyyy-hh:mm";
	public static final String DATE_FORMAT_WH= "dd/MM/yyyy";
	
	public static final double DEFAULT_DENSITY = -1.0;
	public static final String DEFAULT_MAX_TEMP = "NAN";
	public static final String DEFAULT_MIN_TEMP = "NAN";
	public static final String DEFAULT_MAX_HUM = "NAN";
	public static final String DEFAULT_MIN_HUM = "NAN";
	public static final String DEFAULT_RAIN = "NAN";
	public static final String DEFAULT_MAX_RAD = "NAN";
	public static final String DEFAULT_MAX_UV = "NAN";
	public static final String DEFAULT_MAX_EVAP = "NAN";
	
	
	public static final String DATE_FIELD = "date";
	public static final String OCCUPATION_FIELD = "occupation";
	public static final String MAX_TEMP_FIELD = "temperaturamaxima";
	public static final String MIN_TEMP_FIELD = "temperaturaminima";
	public static final String MAX_HUM_FIELD = "humedadmaxima";
	public static final String MIN_HUM_FIELD = "humedadminima";
	public static final String MAX_PRES_FIELD = "presionmaxima";
	public static final String MIN_PRES_FIELD = "presionminima";
	public static final String RAIN_FIELD = "lluvia";
	public static final String MAX_RAD_FIELD = "radiacionsolarmaxima";
	public static final String MAX_UV_FIELD = "indiceuvmaximo";
	public static final String MAX_EVAP_FIELD = "evaporacionmaxima";
}

