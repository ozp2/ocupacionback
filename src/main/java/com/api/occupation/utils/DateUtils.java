package com.api.occupation.utils;

import static com.api.occupation.utils.ConstantsUtil.DATE_FORMAT;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class DateUtils {
	
	public static long convertFromDateToTimeStamp(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date1 = simpleDateFormat.parse(date);
		Timestamp ts = new Timestamp(date1.getTime());
		return ts.getTime() / 1000;
	}
	
	
    public static String getDateFromTimestamp(long timestamp) {
		String date = convertFromTimeStamp(timestamp).toString();
		String day = date.substring(8,10);
    	String month = date.substring(5,7);
    	String year = date.substring(0,4);
		return day + "/" + month + "/" +year;
	}
	
	public static String getHourFromTimestamp(long timestamp) {
		String date = convertFromTimeStamp(timestamp).toString();
		return date.substring(date.indexOf(" ")+1, date.indexOf(" ")+6);
	}

	public static Timestamp convertFromTimeStamp(long timestamp) {
		Timestamp ts1 = new Timestamp(TimeUnit.MILLISECONDS.convert(timestamp, TimeUnit.SECONDS));
		return ts1;
	}
	
	public static int getDayMonths(){
        Calendar fecha = Calendar.getInstance();
        int mes = fecha.get(Calendar.MONTH);
        Calendar cal = new GregorianCalendar(Calendar.YEAR, mes, 1); 
        int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
        return days;
	}
	
	public static int getDayOfMonth(){
		Calendar fecha = Calendar.getInstance();
        return fecha.get(Calendar.DAY_OF_MONTH);
        
	}
	
	public static String getDateToQuery(int day) {
		Calendar fecha = Calendar.getInstance();
		int month = fecha.get(Calendar.MONTH) + 1;
		String queryDay = String.valueOf(day).length() < 2 ? "0" + String.valueOf(day) : String.valueOf(day);
		String queryMonth = String.valueOf(month).length() < 2 ? "0" + String.valueOf(month) : String.valueOf(month);
		return queryDay+"/"+queryMonth+"/"+fecha.get(Calendar.YEAR);
	}
}
