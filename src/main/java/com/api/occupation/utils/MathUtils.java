package com.api.occupation.utils;

public class MathUtils {

	public static double roundDensity(double density) {
		return Math.round((density * 100) * 100d) / 100d;
	}
}
