package com.api.occupation.utils;

import java.util.List;

import com.api.occupation.models.density.Density;

public class DensityUtil {
public static Density getDensityAverage(List<Density> density) {
	Density densityObject =  density.get(0);
	Double densityAverage = 0.0;
	int count = 0;
	if(density.size() > 1) {
		for (int i = 0; i < density.size(); i++) {
			densityAverage = densityAverage + density.get(i).getOccupation();
			count ++;
		}
		densityObject.setOccupation(densityAverage/count);
		return densityObject;
	}
	return density.get(0);
	
}
}
