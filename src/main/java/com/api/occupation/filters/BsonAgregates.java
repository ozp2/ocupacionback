package com.api.occupation.filters;

import static com.api.occupation.utils.ConstantsUtil.DB_DATE;
import static com.api.occupation.utils.ConstantsUtil.DB_BEACH_ID;
import static com.api.occupation.utils.ConstantsUtil.DB_HOUR;
import static com.api.occupation.utils.ConstantsUtil.DB_TIME;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lte;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Sorts;

public class BsonAgregates {

	// Returns all documents for a specific date
	public static AggregateIterable<Document> getDocumentsByDate(MongoCollection<Document> collection, String date) {
		return collection.aggregate(Arrays.asList(match(eq(DB_DATE, date))));
	}

	// Returns last document inserted by day and hour
	public static Document getLastDocumentByDateAndHour(MongoCollection<Document> collection) {
		return collection.aggregate(Arrays.asList(sort(Sorts.descending(DB_DATE, DB_HOUR)))).first();
	}

	// Returns last document inserted by timestamp
	public static Document getLastDocumentByTimestamp(MongoCollection<Document> collection) {
		return collection.aggregate(Arrays.asList(sort(Sorts.descending(DB_TIME)))).first();
	}

	// Returns a document filtering by date and hour
	public static Document getLastDocumentByDateAndHour(MongoCollection<Document> collection, String date,
			String hour) {
		return collection.aggregate(Arrays.asList(match(and(eq(DB_DATE, date), eq(DB_HOUR, hour))))).first();
	}
	
	// Returns last document inserted by timestamp
	public static FindIterable<Document> getLastDocumentByTimestamp(MongoCollection<Document> collection, String beachId, int record) {
		return collection.find(eq(DB_BEACH_ID, beachId)).sort(Sorts.descending(DB_TIME)).limit(record);
	}

	// Returns a list of document filtering by two dates
	public static Document getDocumentsByDateAndHour(MongoCollection<Document> collection, String initDate, String endDate, 
			String initHour, String endHour) {
		return collection.aggregate(Arrays.asList(match(and(and(gte(DB_DATE, initDate),lte(DB_DATE, endDate)), and(gte(DB_HOUR, initHour),lte(DB_HOUR, endHour)))))).first();
	}
	
	// Returns the document with the max value(any field) for an specific day 
	public static Document getMax(MongoCollection<Document> collection, String day, String parameter, String value) {
		Document document = collection
				.aggregate(Arrays.asList(match(eq(parameter, day)), sort(Sorts.descending(value)))).first();
		return document;
	}

	// Returns the document with the min value(any field) for an specific day 
	public static Document getMin(MongoCollection<Document> collection, String day, String parameter, String value) {
		Document document = collection
				.aggregate(Arrays.asList(match(eq(parameter, day)), sort(Sorts.ascending(value)))).first();
		return document;
	}
}
