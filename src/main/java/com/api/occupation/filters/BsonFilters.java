package com.api.occupation.filters;

import static com.mongodb.client.model.Filters.*;

import org.bson.conversions.Bson;

public class BsonFilters {
  public static Bson filterEquals(String field, Object value) {
	  return eq(field, value);
  }
  public static Bson filterGreaterThan(String field, Object value) {
	  return gt(field, value);
  }
  public static Bson filterGreaterThanOrEquals(String field, Object value) {
	  return gte(field, value);
  }
  public static Bson filterLessThan(String field, Object value) {
	  return lt(field, value);
  }
  public static Bson filterLessThanOrEquals(String field, Object value) {
	  return lte(field, value);
  }
  public static Bson filterBetween(String field, Object value1, Object value2) {
	  return and(gt(field, value1),
              lt(field, value2));
  }
  public static Bson filterBetweenInclusive(String field, Object value1, Object value2) {
	  return and(gte(field, value1),
              lte(field, value2));
  }
  
}
