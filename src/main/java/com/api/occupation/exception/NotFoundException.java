package com.api.occupation.exception;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2900664207468909323L;

	public NotFoundException() {
	}

	public NotFoundException(String message) {
		super(message);
	}
}
