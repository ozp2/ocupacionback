package com.api.occupation.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.occupation.models.BeachData;
import com.api.occupation.models.DataHeatMap;
import com.api.occupation.models.DateRange;
import com.api.occupation.models.density.ChartData;
import com.api.occupation.services.DataByZoneService;
import com.api.occupation.services.DensityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("occupation")
@Api(value = "Occupation controller")
public class OccupationController {

	@Autowired
	private DensityService densityService;
	
	@Autowired
	private DataByZoneService dataByZoneService;


	@GetMapping(value = "/density/recordbyzone/{idCam}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Density Record", notes = "Return density record for zones")
	public List<ChartData> getDensityRecordByZone(@PathVariable String idCam, @RequestParam(required = true) Integer record) {
		return densityService.getChartData(idCam, record);
	}

	@GetMapping(value = "/databyzones/{idCam}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Density Data By Date", notes = "Return density data for zones filtering by date")
	public BeachData getDataByZone(@PathVariable String idCam,
			@RequestParam(required = false) String initdatetime, @RequestParam(required = false) String enddatetime) {
		return dataByZoneService.getDataByZone(idCam, new DateRange(initdatetime, enddatetime));
	}

	@GetMapping(value = "/databyzones/heatmap/{idCam}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Heat Map By Date", notes = "Return heat map for zones filtering by date")
	public List<DataHeatMap> getDataByZoneHeatMap(@PathVariable String idCam,
			@RequestParam(required = false) String initdatetime, @RequestParam(required = false) String enddatetime) {
		return dataByZoneService.getHeatMapData(idCam, new DateRange(initdatetime, enddatetime));
	}
}
