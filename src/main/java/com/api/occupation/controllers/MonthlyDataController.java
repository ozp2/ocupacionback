package com.api.occupation.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.occupation.models.DayData;
import com.api.occupation.services.MonthlyDataService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("monthlydata")
@Api(value = "Monthly data controller")
public class MonthlyDataController {


	@Autowired
	private MonthlyDataService monthlyDataService;


	@GetMapping(value = "/density/monthdata/{idCam}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Density Month Data", notes = "Return density month data")
	public List<DayData> getMonthData(@PathVariable String idCam) {
		return monthlyDataService.getMonthData();
	}

}
