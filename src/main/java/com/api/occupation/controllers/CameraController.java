package com.api.occupation.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.occupation.models.CameraSelect;
import com.api.occupation.services.CameraService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("camera")
@Api(value = "Camera Controller")
public class CameraController {

	@Autowired
	private CameraService cameraService;

	@GetMapping(value = "/cameras", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Camera List", notes = "Return all cameras ids")
	public List<CameraSelect> getCameras() {
		return cameraService.getCameras();
	}

}
