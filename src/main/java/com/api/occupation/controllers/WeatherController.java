package com.api.occupation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.occupation.models.mastral.Actuales;
import com.api.occupation.services.WeatherService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("weather")
@Api(value = "Weather Controller")
public class WeatherController {

	@Autowired
	private WeatherService weatherService;
	
	@GetMapping(value = "/weatherbydate/{idCam}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Weather Data By Date", notes = "Return all Mastral Data Information Filtering By Date")
	public Actuales getWeatherByDate(@PathVariable String idCam, @RequestParam(required = false) String initdatetime, @RequestParam(required = false) String enddatetime) {
		return weatherService.getWeatherByDate(idCam, initdatetime, enddatetime );
	}
}
