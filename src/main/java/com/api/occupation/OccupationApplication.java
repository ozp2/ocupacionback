package com.api.occupation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class OccupationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OccupationApplication.class, args);
	}

}
