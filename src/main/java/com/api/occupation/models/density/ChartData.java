package com.api.occupation.models.density;

import java.time.LocalDate;
import java.util.List;

public class ChartData {
	
	private List<LocalDate> date;
	public List<LocalDate> getDate() {
		return date;
	}

	public void setDate(List<LocalDate> date) {
		this.date = date;
	}
	private List<Double> temperature;
	private List<Double> rain;
	private List<Double> humidity;
	private List<Double> occupation;
	
	public ChartData() {
		super();
	}

	public ChartData(List<LocalDate> date, List<Double> temperature, List<Double> rain, List<Double> humidity,
			List<Double> occupation) {
		super();
		this.date = date;
		this.temperature = temperature;
		this.rain = rain;
		this.humidity = humidity;
		this.occupation = occupation;
	}

	public List<Double> getTemperature() {
		return temperature;
	}
	public void setTemperature(List<Double> temperature) {
		this.temperature = temperature;
	}

	public List<Double> getOccupation() {
		return occupation;
	}
	public void setOccupation(List<Double> occupation) {
		this.occupation = occupation;
	}
	public List<Double> getRain() {
		return rain;
	}
	public void setRain(List<Double> rain) {
		this.rain = rain;
	}
	public List<Double> getHumidity() {
		return humidity;
	}
	public void setHumidity(List<Double> humidity) {
		this.humidity = humidity;
	}
	@Override
	public String toString() {
		return "ChartData [dateTime=" + date + ", occupation=" + occupation + "]";
	}
}
