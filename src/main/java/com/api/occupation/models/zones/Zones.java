package com.api.occupation.models.zones;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "_idcam", "zonelist" })
public class Zones {

	@JsonProperty("_idcam")
	private String idcam;
	@JsonProperty("zonelist")
	private List<ZoneList> zonelist = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("_idcam")
	public String getIdcam() {
		return idcam;
	}

	@JsonProperty("_idcam")
	public void setIdcam(String idcam) {
		this.idcam = idcam;
	}

	@JsonProperty("zonelist")
	public List<ZoneList> getZonelist() {
		return zonelist;
	}

	@JsonProperty("zonelist")
	public void setZonelist(List<ZoneList> zonelist) {
		this.zonelist = zonelist;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}