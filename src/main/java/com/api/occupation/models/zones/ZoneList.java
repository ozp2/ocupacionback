package com.api.occupation.models.zones;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "lat11", "lat12", "lat21", "lat22", "lng11", "lng12", "lng21", "lng22", "nameij", "zoname" })
public class ZoneList {

	@JsonProperty("lat11")
	private Double lat11;
	@JsonProperty("lat12")
	private Double lat12;
	@JsonProperty("lat21")
	private Double lat21;
	@JsonProperty("lat22")
	private Double lat22;
	@JsonProperty("lng11")
	private Double lng11;
	@JsonProperty("lng12")
	private Double lng12;
	@JsonProperty("lng21")
	private Double lng21;
	@JsonProperty("lng22")
	private Double lng22;
	@JsonProperty("nameij")
	private String nameij;
	@JsonProperty("zoname")
	private String zoname;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("lat11")
	public Double getLat11() {
		return lat11;
	}

	@JsonProperty("lat11")
	public void setLat11(Double lat11) {
		this.lat11 = lat11;
	}

	@JsonProperty("lat12")
	public Double getLat12() {
		return lat12;
	}

	@JsonProperty("lat12")
	public void setLat12(Double lat12) {
		this.lat12 = lat12;
	}

	@JsonProperty("lat21")
	public Double getLat21() {
		return lat21;
	}

	@JsonProperty("lat21")
	public void setLat21(Double lat21) {
		this.lat21 = lat21;
	}

	@JsonProperty("lat22")
	public Double getLat22() {
		return lat22;
	}

	@JsonProperty("lat22")
	public void setLat22(Double lat22) {
		this.lat22 = lat22;
	}

	@JsonProperty("lng11")
	public Double getLng11() {
		return lng11;
	}

	@JsonProperty("lng11")
	public void setLng11(Double lng11) {
		this.lng11 = lng11;
	}

	@JsonProperty("lng12")
	public Double getLng12() {
		return lng12;
	}

	@JsonProperty("lng12")
	public void setLng12(Double lng12) {
		this.lng12 = lng12;
	}

	@JsonProperty("lng21")
	public Double getLng21() {
		return lng21;
	}

	@JsonProperty("lng21")
	public void setLng21(Double lng21) {
		this.lng21 = lng21;
	}

	@JsonProperty("lng22")
	public Double getLng22() {
		return lng22;
	}

	@JsonProperty("lng22")
	public void setLng22(Double lng22) {
		this.lng22 = lng22;
	}

	@JsonProperty("nameij")
	public String getNameij() {
		return nameij;
	}

	@JsonProperty("nameij")
	public void setNameij(String nameij) {
		this.nameij = nameij;
	}

	@JsonProperty("zoname")
	public String getZoname() {
		return zoname;
	}

	@JsonProperty("zoname")
	public void setZoname(String zoname) {
		this.zoname = zoname;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}