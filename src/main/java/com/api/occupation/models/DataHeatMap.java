package com.api.occupation.models;

public class DataHeatMap {
	
	private Double latitude;
	private Double longitude;
	private Double density;
	
	public DataHeatMap() {
		super();
	}
	
	public DataHeatMap(Double latitude, Double longitude, Double density) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.density = density;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getDensity() {
		return density;
	}

	public void setDensity(Double density) {
		this.density = density;
	}
}