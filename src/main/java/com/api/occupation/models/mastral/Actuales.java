package com.api.occupation.models.mastral;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="actuales")
public class Actuales {
	private String lluviaultimahora;

    private String puntoderocio;

    private String velocidadviento;

    private String temperatura;

    private String humedad;

    private String intensidadlluvia;

    private String lluvia;

    private String presion;

    private String direccionviento;
    
    private String indiceuv;

    private String radiacionsolar;

    private String evaporacion;

    public String getIndiceuv() {
		return indiceuv;
	}

	public void setIndiceuv(String indiceuv) {
		this.indiceuv = indiceuv;
	}

	public String getRadiacionsolar() {
		return radiacionsolar;
	}

	public void setRadiacionsolar(String radiacionsolar) {
		this.radiacionsolar = radiacionsolar;
	}

	public String getEvaporacion() {
		return evaporacion;
	}

	public void setEvaporacion(String evaporacion) {
		this.evaporacion = evaporacion;
	}

	public String getLluviaultimahora ()
    {
        return lluviaultimahora;
    }

    public void setLluviaultimahora (String lluviaultimahora)
    {
        this.lluviaultimahora = lluviaultimahora;
    }

    public String getPuntoderocio ()
    {
        return puntoderocio;
    }

    public void setPuntoderocio (String puntoderocio)
    {
        this.puntoderocio = puntoderocio;
    }

    public String getVelocidadviento ()
    {
        return velocidadviento;
    }

    public void setVelocidadviento (String velocidadviento)
    {
        this.velocidadviento = velocidadviento;
    }

    public String getTemperatura ()
    {
        return temperatura;
    }

    public void setTemperatura (String temperatura)
    {
        this.temperatura = temperatura;
    }

    public String getHumedad ()
    {
        return humedad;
    }

    public void setHumedad (String humedad)
    {
        this.humedad = humedad;
    }

    public String getIntensidadlluvia ()
    {
        return intensidadlluvia;
    }

    public void setIntensidadlluvia (String intensidadlluvia)
    {
        this.intensidadlluvia = intensidadlluvia;
    }

    public String getLluvia ()
    {
        return lluvia;
    }

    public void setLluvia (String lluvia)
    {
        this.lluvia = lluvia;
    }

    public String getPresion ()
    {
        return presion;
    }

    public void setPresion (String presion)
    {
        this.presion = presion;
    }

    public String getDireccionviento ()
    {
        return direccionviento;
    }

    public void setDireccionviento (String direccionviento)
    {
        this.direccionviento = direccionviento;
    }
}
