package com.api.occupation.models.mastral;

public class WeatherMastral {
	private Proyecto_Mastral_XML_V1 Proyecto_Mastral_XML_V1;
	
	public WeatherMastral() {
		super();
	}
	
	public WeatherMastral(Proyecto_Mastral_XML_V1 proyecto_Mastral_XML_V1) {
		super();
		Proyecto_Mastral_XML_V1 = proyecto_Mastral_XML_V1;
	}

	public Proyecto_Mastral_XML_V1 getProyecto_Mastral_XML_V1() {
		return Proyecto_Mastral_XML_V1;
	}

	public void setProyecto_Mastral_XML_V1(Proyecto_Mastral_XML_V1 Proyecto_Mastral_XML_V1) {
		this.Proyecto_Mastral_XML_V1 = Proyecto_Mastral_XML_V1;
	}
}
