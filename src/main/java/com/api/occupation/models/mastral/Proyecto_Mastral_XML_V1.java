package com.api.occupation.models.mastral;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;

@ApiModel("Mastral XML Data")
@XmlRootElement(name="Proyecto_Mastral_XML_V1")
public class Proyecto_Mastral_XML_V1 {
	private String fecha;

	private Anuales anuales;

	private Mensuales mensuales;

	private String codigoestacion;

	private String hora;

	private Actuales actuales;

	private String estacion;

	private String autor;

	private Diarios diarios;

	public Proyecto_Mastral_XML_V1() {
		super();
	}

	public Proyecto_Mastral_XML_V1(String fecha, Anuales anuales, Mensuales mensuales, String codigoestacion,
			String hora, Actuales actuales, String estacion, String autor, Diarios diarios) {
		super();
		this.fecha = fecha;
		this.anuales = anuales;
		this.mensuales = mensuales;
		this.codigoestacion = codigoestacion;
		this.hora = hora;
		this.actuales = actuales;
		this.estacion = estacion;
		this.autor = autor;
		this.diarios = diarios;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Anuales getAnuales() {
		return anuales;
	}

	public void setAnuales(Anuales anuales) {
		this.anuales = anuales;
	}

	public Mensuales getMensuales() {
		return mensuales;
	}

	public void setMensuales(Mensuales mensuales) {
		this.mensuales = mensuales;
	}

	public String getCodigoestacion() {
		return codigoestacion;
	}

	public void setCodigoestacion(String codigoestacion) {
		this.codigoestacion = codigoestacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Actuales getActuales() {
		return actuales;
	}

	public void setActuales(Actuales actuales) {
		this.actuales = actuales;
	}

	public String getEstacion() {
		return estacion;
	}

	public void setEstacion(String estacion) {
		this.estacion = estacion;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Diarios getDiarios() {
		return diarios;
	}

	public void setDiarios(Diarios diarios) {
		this.diarios = diarios;
	}

}
