package com.api.occupation.models.mastral;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="diarios")
public class Diarios {
	private String presionminima;

    private String temperaturaminima;

    private String humedadminima;

    private String presionmaxima;

    private String temperaturamaxima;

    private String rachaviento;

    private String humedadmaxima;

    private String intensidadlluvia;

    private String lluvia;

    private String puntoderociomaximo;

    private String puntoderociominimo;
    
	private String indiceuvmaximo;
	
	private String radiacionsolarmaxima;
	
	private String evaporacionmaxima;

    public String getIndiceuvmaximo() {
		return indiceuvmaximo;
	}

	public void setIndiceuvmaximo(String indiceuvmaximo) {
		this.indiceuvmaximo = indiceuvmaximo;
	}

	public String getRadiacionsolarmaxima() {
		return radiacionsolarmaxima;
	}

	public void setRadiacionsolarmaxima(String radiacionsolarmaxima) {
		this.radiacionsolarmaxima = radiacionsolarmaxima;
	}

	public String getEvaporacionmaxima() {
		return evaporacionmaxima;
	}

	public void setEvaporacionmaxima(String evaporacionmaxima) {
		this.evaporacionmaxima = evaporacionmaxima;
	}

	public String getPresionminima ()
    {
        return presionminima;
    }

    public void setPresionminima (String presionminima)
    {
        this.presionminima = presionminima;
    }

    public String getTemperaturaminima ()
    {
        return temperaturaminima;
    }

    public void setTemperaturaminima (String temperaturaminima)
    {
        this.temperaturaminima = temperaturaminima;
    }

    public String getHumedadminima ()
    {
        return humedadminima;
    }

    public void setHumedadminima (String humedadminima)
    {
        this.humedadminima = humedadminima;
    }

    public String getPresionmaxima ()
    {
        return presionmaxima;
    }

    public void setPresionmaxima (String presionmaxima)
    {
        this.presionmaxima = presionmaxima;
    }

    public String getTemperaturamaxima ()
    {
        return temperaturamaxima;
    }

    public void setTemperaturamaxima (String temperaturamaxima)
    {
        this.temperaturamaxima = temperaturamaxima;
    }

    public String getRachaviento ()
    {
        return rachaviento;
    }

    public void setRachaviento (String rachaviento)
    {
        this.rachaviento = rachaviento;
    }

    public String getHumedadmaxima ()
    {
        return humedadmaxima;
    }

    public void setHumedadmaxima (String humedadmaxima)
    {
        this.humedadmaxima = humedadmaxima;
    }

    public String getIntensidadlluvia ()
    {
        return intensidadlluvia;
    }

    public void setIntensidadlluvia (String intensidadlluvia)
    {
        this.intensidadlluvia = intensidadlluvia;
    }

    public String getLluvia ()
    {
        return lluvia;
    }

    public void setLluvia (String lluvia)
    {
        this.lluvia = lluvia;
    }

    public String getPuntoderociomaximo ()
    {
        return puntoderociomaximo;
    }

    public void setPuntoderociomaximo (String puntoderociomaximo)
    {
        this.puntoderociomaximo = puntoderociomaximo;
    }

    public String getPuntoderociominimo ()
    {
        return puntoderociominimo;
    }

    public void setPuntoderociominimo (String puntoderociominimo)
    {
        this.puntoderociominimo = puntoderociominimo;
    }
}
