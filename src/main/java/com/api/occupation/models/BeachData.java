package com.api.occupation.models;

import java.time.LocalDateTime;
import java.util.List;

public class BeachData {
	private List<ZoneData> zoneDataList;
	private Double totalDensity;
	private LocalDateTime dateTime;
	public BeachData() {
		super();
	}
	public BeachData(List<ZoneData> zoneDataList, Double totalDensity, LocalDateTime dateTime) {
		super();
		this.zoneDataList = zoneDataList;
		this.totalDensity = totalDensity;
		this.dateTime = dateTime;
	}
	public List<ZoneData> getZoneDataList() {
		return zoneDataList;
	}
	public void setZoneDataList(List<ZoneData> zoneDataList) {
		this.zoneDataList = zoneDataList;
	}
	public Double getTotalDensity() {
		return totalDensity;
	}
	public void setTotalDensity(Double totalDensity) {
		this.totalDensity = totalDensity;
	}
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	@Override
	public String toString() {
		return "BeachData [zoneDataList=" + zoneDataList + ", totalDensity=" + totalDensity + ", dateTime=" + dateTime
				+ "]";
	}
}
