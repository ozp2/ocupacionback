package com.api.occupation.models;

import com.api.occupation.models.zones.ZoneList;

public class ZoneData {
	private Double density;
	private ZoneList zoneList;
	public ZoneData() {
		super();
	}
	public ZoneData(double density, ZoneList zoneList) {
		super();
		this.density = density;
		this.zoneList = zoneList;
	}
	public double getDensity() {
		return density;
	}
	public void setDensity(double density) {
		this.density = density;
	}
	public ZoneList getZoneList() {
		return zoneList;
	}
	public void setZoneList(ZoneList zoneList) {
		this.zoneList = zoneList;
	}
}