package com.api.occupation.models;

public class DayData {
	
	private int day;
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	private Double density;
	private String tempMin;
	private String tempMax;
	private String humMin;
	private String humMax;
	private String evapMax;
	private String radMax;
	private String uvMax;
	private String rain;
	public Double getDensity() {
		return density;
	}

	public void setDensity(Double density) {
		this.density = density;
	}

	public String getTempMin() {
		return tempMin;
	}

	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}

	public String getTempMax() {
		return tempMax;
	}

	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}


	public String getHumMin() {
		return humMin;
	}

	public void setHumMin(String humMin) {
		this.humMin = humMin;
	}

	public String getHumMax() {
		return humMax;
	}

	public void setHumMax(String humMax) {
		this.humMax = humMax;
	}

	public String getEvapMax() {
		return evapMax;
	}

	public void setEvapMax(String evapMax) {
		this.evapMax = evapMax;
	}

	public String getRadMax() {
		return radMax;
	}

	public void setRadMax(String radMax) {
		this.radMax = radMax;
	}

	public String getUvMax() {
		return uvMax;
	}

	public void setUvMax(String uvMax) {
		this.uvMax = uvMax;
	}

	public String getRain() {
		return rain;
	}

	public void setRain(String rain) {
		this.rain = rain;
	}


}
