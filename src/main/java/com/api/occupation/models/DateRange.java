package com.api.occupation.models;

public class DateRange {
	
	private String initdatetime;
	private String enddatetime;

	public String getInitdatetime() {
		return initdatetime;
	}

	public void setInitdatetime(String initdatetime) {
		this.initdatetime = initdatetime;
	}

	public String getEnddatetime() {
		return enddatetime;
	}

	public void setEnddatetime(String enddatetime) {
		this.enddatetime = enddatetime;
	}

	public DateRange() {
		super();
	}

	public DateRange(String initdatetime, String enddatetime) {
		super();
		this.initdatetime = initdatetime;
		this.enddatetime = enddatetime;
	}

}
