package com.api.occupation.models;

import com.api.occupation.models.mastral.Actuales;

public class Weather {
	
	private Actuales actuales;
	private String fecha;
	private String hora;
	private String estacion;

	public Weather() {
		super();
	}

	public Weather(Actuales actuales, String fecha, String hora, String estacion) {
		super();
		this.actuales = actuales;
		this.fecha = fecha;
		this.hora = hora;
		this.estacion = estacion;
	}

	public Actuales getActuales() {
		return actuales;
	}

	public void setActuales(Actuales actuales) {
		this.actuales = actuales;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getEstacion() {
		return estacion;
	}

	public void setEstacion(String estacion) {
		this.estacion = estacion;
	}

}
