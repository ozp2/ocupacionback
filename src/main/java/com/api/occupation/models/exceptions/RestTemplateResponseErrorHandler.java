/**
 *
 */
package com.api.occupation.models.exceptions;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import com.api.occupation.exception.NotFoundException;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

	@Override
	public boolean hasError(ClientHttpResponse httpResponse) throws IOException {

		return ((httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR)
				|| (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR));
	}

	@Override
	public void handleError(ClientHttpResponse httpResponse) throws IOException {
		if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
			switch (httpResponse.getStatusCode()) {
			case INTERNAL_SERVER_ERROR:
				throw new IOException("INTERNAL_SERVER_ERROR");
			default:
				throw new IOException("OTHER_REQUEST_EXCEPTION");
			}
			// handle SERVER_ERROR
		} else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
			// handle CLIENT_ERROR
			switch (httpResponse.getStatusCode()) {
			case NOT_FOUND:
				throw new NotFoundException();
			case BAD_REQUEST:
				throw new IOException("BAD_REQUEST");
			default:
				throw new IOException("OTHER_REQUEST_EXCEPTION");
			}
		}
	}
}
